import { GetAllUserUseCase } from "../../../../scheduleContext/user/application/getAllUser";
import { UserEntity } from "../../../../scheduleContext/user/domain";
import { fakeUser } from "../../../user/domain/userFakes";
import Container from "typedi";
import { TypeOrmUserRepository } from "../../../../scheduleContext/user/infrastructure";
import { createConnection } from "typeorm";

describe("GetAllUser UseCase", () => {
  let getAllUserUseCase: GetAllUserUseCase;
  let userEntity: UserEntity;

  beforeAll(async () => {
    await createConnection();
    getAllUserUseCase = new GetAllUserUseCase(new TypeOrmUserRepository());
    userEntity = UserEntity.createToSave(fakeUser);
  });

  it("Get array of User", async () => {
    const spyRepository = jest
      .spyOn((getAllUserUseCase as any).userRepository, "getAll")
      .mockReturnValueOnce([userEntity]);
    const arrayUser = await getAllUserUseCase.getAllUser();

    expect(arrayUser).toStrictEqual([userEntity]);
    expect(spyRepository).toHaveBeenCalledTimes(1);
  });
});
