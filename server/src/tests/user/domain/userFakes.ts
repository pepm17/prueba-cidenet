const completeNameUser = {
  firstName: "fn",
  otherName: "on",
  firstSurname: "fs",
  lastSurname: "ls",
};
const admissionDate = new Date();
const updatedAt = new Date();
const createdAt = new Date();
const country = {
  id: "1",
  name: "colombia",
  domain: "domain",
};
const userIdentification = {
  identificationNumber: "IN",
  identificationType: "IT",
};
const area = {
  id: "1",
  name: "area1",
};

export const fakeToSaveInDb = {
  ...completeNameUser,
  email: "fn.fs.2@domain",
  employmentCountry: country.id as unknown as number,
  area: area.id as unknown as number,
  ...userIdentification,
  admissionDate,
  status: true,
};

export const fakeUser = {
  id: "1",
  completeNameUser,
  country,
  userIdentification,
  area,
  admissionDate,
  status: true,
};

export const fakeArrayUserJson = {
  ...fakeUser,
  createdAt,
  updatedAt,
};

export const fakeUserJsonAll = {
  id: "1",
  ...completeNameUser,
  employmentCountry: country,
  ...userIdentification,
  area,
  admissionDate,
  status: true,
  email: "fn.fs.2@domain",
  createdAt,
  updatedAt,
};

export const fakeUserJson = {
  id: "1",
  ...completeNameUser,
  employmentCountry: country.name,
  ...userIdentification,
  area: area.name,
  admissionDate,
  status: true,
  email: "fn.fs.2@domain",
  createdAt: undefined,
  updatedAt: undefined,
};
