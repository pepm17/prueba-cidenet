import { UserEntity } from "../../../scheduleContext/user/domain";
import {
  fakeArrayUserJson,
  fakeToSaveInDb,
  fakeUser,
  fakeUserJson,
  fakeUserJsonAll,
} from "./userFakes";

describe("User Entity", () => {
  let userEntity: UserEntity;

  beforeAll(() => {
    userEntity = UserEntity.createToSave(fakeUser);
  });

  it("Generate an email", () => {
    userEntity.generateEmail(".2");
    const { email } = userEntity.toJson();
    const fakeEmail = "fn.fs.2@domain";
    expect(email).toBe(fakeEmail);
  });

  it("Get json from array", () => {
    const arrayUser = [userEntity];
    const user = UserEntity.toJsonArray(arrayUser);
    expect(user).toStrictEqual([fakeUserJson]);
  });

  it("Create entity from array", () => {
    const user = UserEntity.createToSave(fakeArrayUserJson);
    user.generateEmail(".2");
    const userEntity2 = [user];
    const response = UserEntity.createFromArray([fakeUserJsonAll]);
    expect(response).toStrictEqual(userEntity2);
  });

  it("Validate return data to validate email", () => {
    const data = {
      partialEmail: "fn.fs",
      domain: "domain",
    };
    const returnData = userEntity.toValidateIfExistEmail();
    expect(returnData).toStrictEqual(data);
  });

  it("Validate return data to saveInDb", () => {
    const returnData = userEntity.toSaveInDb();
    expect(returnData).toStrictEqual(fakeToSaveInDb);
  });
});
