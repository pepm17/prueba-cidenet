import { Query, Command } from ".";

export interface CommandQueryBus {
  handle(commandQuery: Command | Query): Promise<any>;
}
