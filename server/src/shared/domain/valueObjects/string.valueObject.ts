export class StringValueObject {
  constructor(private value: string) {}

  getValue(): string {
    return this.value;
  }
}
