import {
  CreateUserCommandHandler,
  CreateUserUseCase,
} from "@ScheduleContext/user/application/createUserUseCase";
import {
  DeleteUserCommandHandler,
  DeleteUserUseCase,
} from "@ScheduleContext/user/application/deleteUserUseCase";
import {
  EditUserCommandHandler,
  EditUserUseCase,
} from "@ScheduleContext/user/application/editUserUseCase";
import {
  GetAllUserQueryHandler,
  GetAllUserUseCase,
} from "@ScheduleContext/user/application/getAllUser";
import {
  Command,
  CommandQueryBus,
  Query,
  Handler,
} from "@Shared/domain/contracts";
import { NotFoundError } from "routing-controllers";
import Container, { ContainerInstance } from "typedi";

export class TypediCommandQueryBus implements CommandQueryBus {
  constructor(private readonly container: ContainerInstance) {
    this.addHandler();
  }

  async handle(commandQuery: Command | Query): Promise<any> {
    const constructorName = commandQuery.constructor.name;

    if (!this.container.has(constructorName)) {
      throw new NotFoundError("Handler for command or query not found");
    }

    const handler = this.container.get<Handler<any>>(constructorName);
    return handler.handler(commandQuery);
  }

  private addHandler() {
    this.addCommandHandler();
    this.addQueryHandler();
  }

  private addQueryHandler() {
    this.container.set(
      "GetAllUserQuery",
      new GetAllUserQueryHandler(
        Container.get<GetAllUserUseCase>(GetAllUserUseCase)
      )
    );
  }

  private addCommandHandler() {
    this.container.set(
      "CreateUserCommand",
      new CreateUserCommandHandler(
        Container.get<CreateUserUseCase>(CreateUserUseCase)
      )
    );

    this.container.set(
      "DeleteUserCommand",
      new DeleteUserCommandHandler(
        Container.get<DeleteUserUseCase>(DeleteUserUseCase)
      )
    );

    this.container.set(
      "EditUserCommand",
      new EditUserCommandHandler(
        Container.get<EditUserUseCase>(EditUserUseCase)
      )
    );
  }
}
