import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1621557568360 implements MigrationInterface {
  name = "Migration1621557568360";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "countries" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "domain" character varying NOT NULL, "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), CONSTRAINT "PK_b2d7006793e8697ab3ae2deff18" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "users" ("id" SERIAL NOT NULL, "firstName" character varying NOT NULL, "otherName" character varying NOT NULL, "firstSurname" character varying NOT NULL, "lastSurname" character varying NOT NULL, "identificationType" character varying NOT NULL, "identificationNumber" character varying NOT NULL, "email" character varying NOT NULL, "status" boolean NOT NULL, "admissionDate" TIMESTAMP NOT NULL, "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), "employmentCountryId" integer, "areaId" integer, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "area" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "createdAt" TIMESTAMP DEFAULT now(), "updatedAt" TIMESTAMP DEFAULT now(), CONSTRAINT "PK_39d5e4de490139d6535d75f42ff" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_0379a35295ed04fd56c4a3d2da1" FOREIGN KEY ("employmentCountryId") REFERENCES "countries"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "users" ADD CONSTRAINT "FK_a1d8c7107473ecf2935f59491b5" FOREIGN KEY ("areaId") REFERENCES "area"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `INSERT INTO countries (name, domain) VALUES ('Colombia', 'cidenet.com.co')`
    );
    await queryRunner.query(
      `INSERT INTO "countries" ("name", "domain") VALUES ('USA', 'cidenet.com.us')`
    );
    await queryRunner.query(
      `INSERT INTO "area" ("name") VALUES ('Administración')`
    );
    await queryRunner.query(
      `INSERT INTO "area" ("name") VALUES ('Financiera')`
    );
    await queryRunner.query(`INSERT INTO "area" ("name") VALUES ('Compras')`);
    await queryRunner.query(
      `INSERT INTO "area" ("name") VALUES ('Infraestructura')`
    );
    await queryRunner.query(`INSERT INTO "area" ("name") VALUES ('Operación')`);
    await queryRunner.query(
      `INSERT INTO "area" ("name") VALUES ('Talento humano')`
    );
    await queryRunner.query(
      `INSERT INTO "area" ("name") VALUES ('Servicios Varios')`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_a1d8c7107473ecf2935f59491b5"`
    );
    await queryRunner.query(
      `ALTER TABLE "users" DROP CONSTRAINT "FK_0379a35295ed04fd56c4a3d2da1"`
    );
    await queryRunner.query(`DROP TABLE "area"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(`DROP TABLE "countries"`);
  }
}
