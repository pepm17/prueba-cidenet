import { CreateUserDTO } from "../../domain";
import { Command } from "../../../../shared/domain/contracts";

export class CreateUserCommand implements Command {
  constructor(
    private firstName: string,
    private othersName: string,
    private firstSurname: string,
    private lastSurname: string,
    private identificationType: string,
    private identificationNumber: string,
    private status: boolean,
    private countryId: string,
    private areaId: string,
    private admissionDate: Date
  ) {}

  static create(data: any) {
    return new this(
      data.firstName,
      data.othersName,
      data.firstSurname,
      data.lastSurname,
      data.identificationType,
      data.identificationNumber,
      data.status,
      data.countryId,
      data.areaId,
      data.admissionDate
    );
  }

  toJson(): CreateUserDTO {
    return {
      completeNameUser: {
        firstName: this.firstName,
        otherName: this.othersName,
        firstSurname: this.firstSurname,
        lastSurname: this.lastSurname,
      },
      userIdentification: {
        identificationType: this.identificationType,
        identificationNumber: this.identificationNumber,
      },
      status: this.status,
      countryId: this.countryId,
      areaId: this.areaId,
      admissionDate: this.admissionDate,
    };
  }
}
