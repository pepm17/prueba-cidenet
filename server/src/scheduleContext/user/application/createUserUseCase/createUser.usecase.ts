import {
  GetAreaByIdCommand,
  GetAreaByIdCommandHandler,
} from "../../../area/application/getAreaById";
import {
  GetCountryByIdCommand,
  GetCountryByIdCommandHandler,
} from "../../../country/application/getCountryById";
import { CreateUserDTO, UserEntity } from "../../domain";
import { UserRepository } from "../../domain/contracts";
import { Inject } from "typedi";

export class CreateUserUseCase {
  constructor(
    @Inject("UserRepository") private userRepository: UserRepository,
    @Inject("GetCountryByIdCommandHandler")
    private getCountryByIdCommandHandler: GetCountryByIdCommandHandler,
    @Inject("GetAreaByIdCommandHandler")
    private getAreaByIdCommandHandler: GetAreaByIdCommandHandler
  ) {}

  async createUser(user: CreateUserDTO) {
    const validateIdentification =
      await this.userRepository.validateIdentification({
        identificationType: user.userIdentification.identificationType,
        identificationNumber: user.userIdentification.identificationNumber,
      });

    if (validateIdentification) {
      throw new Error("Identification already exist");
    }

    const commandCountry = new GetCountryByIdCommand(user.countryId);
    const country = await this.getCountryByIdCommandHandler.handler(
      commandCountry
    );

    const commandArea = new GetAreaByIdCommand(user.countryId);
    const area = await this.getAreaByIdCommandHandler.handler(commandArea);

    const buildData = {
      country: {
        ...country.toJson(),
      },
      area: {
        ...area.toJson(),
      },
      ...user,
    };

    const data = UserEntity.createToSave(buildData);

    const toValidate = data.toValidateIfExistEmail();
    const repe = await this.userRepository.validateEmail(toValidate);

    data.generateEmail(repe.length < 1 ? "" : `.${repe.length}`);
    this.userRepository.create(data);
  }
}
