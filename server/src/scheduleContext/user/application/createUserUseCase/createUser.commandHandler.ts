import { Handler } from "../../../../shared/domain/contracts";
import { CreateUserCommand, CreateUserUseCase } from ".";

export class CreateUserCommandHandler implements Handler<void> {
  constructor(private createUserUseCase: CreateUserUseCase) {}

  async handler(createUserCommand: CreateUserCommand): Promise<void> {
    await this.createUserUseCase.createUser(createUserCommand.toJson());
  }
}
