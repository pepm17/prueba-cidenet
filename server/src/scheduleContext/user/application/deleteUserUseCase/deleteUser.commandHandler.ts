import { Handler } from "../../../../shared/domain/contracts";
import { DeleteUserCommand, DeleteUserUseCase } from ".";
import { UserEntity } from "../../domain";
import { Id } from "../../domain/valueObjects";

export class DeleteUserCommandHandler implements Handler<void> {
  constructor(private deleteUserUseCase: DeleteUserUseCase) {}

  async handler(command: DeleteUserCommand): Promise<void> {
    const { id } = command.toJson();
    this.deleteUserUseCase.deleteUser(new Id(id));
  }
}
