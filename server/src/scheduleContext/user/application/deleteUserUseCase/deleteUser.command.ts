import { Command } from "../../../../shared/domain/contracts";

export class DeleteUserCommand implements Command {
  constructor(private id: string) {}

  toJson() {
    return {
      id: this.id,
    };
  }
}
