import { UserRepository } from "../../domain/contracts";
import { Inject } from "typedi";
import { Id } from "../../domain/valueObjects";

export class DeleteUserUseCase {
  constructor(
    @Inject("UserRepository") private userRepository: UserRepository
  ) {}

  async deleteUser(id: Id) {
    return this.userRepository.delete(id);
  }
}
