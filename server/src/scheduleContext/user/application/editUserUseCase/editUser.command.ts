import { EditUserDTO } from "@ScheduleContext/user/domain";
import { Command } from "../../../../shared/domain/contracts";

export class EditUserCommand implements Command {
  constructor(
    private id: string,
    private firstName: string,
    private othersName: string,
    private firstSurname: string,
    private lastSurname: string,
    private identificationType: string,
    private identificationNumber: string,
    private status: boolean,
    private countryId: string,
    private areaId: string
  ) {}

  static create(data: any, id: string) {
    return new this(
      id,
      data.firstName,
      data.othersName,
      data.firstSurname,
      data.lastSurname,
      data.identificationType,
      data.identificationNumber,
      data.status,
      data.countryId,
      data.areaId
    );
  }

  toJson(): EditUserDTO {
    return {
      id: this.id,
      completeNameUser: {
        firstName: this.firstName,
        otherName: this.othersName,
        firstSurname: this.firstSurname,
        lastSurname: this.lastSurname,
      },
      userIdentification: {
        identificationType: this.identificationType,
        identificationNumber: this.identificationNumber,
      },
      status: this.status,
      countryId: this.countryId,
      areaId: this.areaId,
    };
  }
}
