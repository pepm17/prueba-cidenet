import { UserRepository } from "../../domain/contracts";
import { Inject } from "typedi";
import { EditUserDTO, UserEntity } from "../../domain";
import {
  GetCountryByIdCommand,
  GetCountryByIdCommandHandler,
} from "../../../country/application/getCountryById";
import {
  GetAreaByIdCommand,
  GetAreaByIdCommandHandler,
} from "../../../area/application/getAreaById";

export class EditUserUseCase {
  constructor(
    @Inject("UserRepository") private userRepository: UserRepository,
    @Inject("GetCountryByIdCommandHandler")
    private getCountryByIdCommandHandler: GetCountryByIdCommandHandler,
    @Inject("GetAreaByIdCommandHandler")
    private getAreaByIdCommandHandler: GetAreaByIdCommandHandler
  ) {}

  async editUser(user: EditUserDTO) {
    const commandCountry = new GetCountryByIdCommand(user.countryId);
    const country = await this.getCountryByIdCommandHandler.handler(
      commandCountry
    );

    const commandArea = new GetAreaByIdCommand(user.countryId);
    const area = await this.getAreaByIdCommandHandler.handler(commandArea);

    const buildData = {
      country: {
        ...country.toJson(),
      },
      area: {
        ...area.toJson(),
      },
      ...user,
    };
    const data = UserEntity.createToSave(buildData);
    const toValidate = data.toValidateIfExistEmail();
    const repe = await this.userRepository.validateEmail(toValidate);
    data.generateEmail(repe.length < 1 ? "" : `.${repe.length}`);
    this.userRepository.edit(data);
  }
}
