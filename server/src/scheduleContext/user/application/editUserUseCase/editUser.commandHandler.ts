import { Handler } from "../../../../shared/domain/contracts";
import { EditUserCommand, EditUserUseCase } from ".";
import { UserEntity } from "@ScheduleContext/user/domain";

export class EditUserCommandHandler implements Handler<void> {
  constructor(private editUserUseCase: EditUserUseCase) {}

  async handler(command: EditUserCommand): Promise<void> {
    this.editUserUseCase.editUser(command.toJson());
  }
}
