import { Handler } from "../../../../shared/domain/contracts";
import { GetAllUserQuery, GetAllUserUseCase } from ".";
import { UserEntity } from "../../domain";

export class GetAllUserQueryHandler implements Handler<any> {
  constructor(private getAllUserUseCase: GetAllUserUseCase) {}

  async handler(_query: GetAllUserQuery): Promise<any> {
    const data = await this.getAllUserUseCase.getAllUser();
    return UserEntity.toJsonArray(data);
  }
}
