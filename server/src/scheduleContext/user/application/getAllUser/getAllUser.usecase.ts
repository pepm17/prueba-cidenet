import { UserRepository } from "../../domain/contracts";
import { Inject } from "typedi";

export class GetAllUserUseCase {
  constructor(
    @Inject("UserRepository") private userRepository: UserRepository
  ) {}

  async getAllUser() {
    return this.userRepository.getAll();
  }
}
