import { AreaEntity } from "../../area/domain";
import { CountryEntity } from "../../country/domain";
import {
  Email,
  AdmissionDate,
  CreatedAt,
  Status,
  UpdatedAt,
  Id,
  CompleteNameUser,
  UserIdentification,
} from "./valueObjects";

export class UserEntity {
  constructor(
    private id: Id,
    private completeNameUser: CompleteNameUser,
    private employmentCountry: CountryEntity,
    private userIdentification: UserIdentification,
    private area: AreaEntity,
    private admissionDate: AdmissionDate,
    private status: Status = new Status(true),
    private email: Email = new Email(""),
    private createdAt?: CreatedAt,
    private updatedAt?: UpdatedAt
  ) {}

  /*static create(data: any): UserEntity {
    return new this(
      data.id ? new Id(data.id) : new Id(""),
      CompleteNameUser.create({ ...data.completeNameUser }),
      CountryEntity.create({ ...data.country }),
      UserIdentification.create({ ...data.userIdentification }),
      AreaEntity.create({ ...data.area }),
      new AdmissionDate(data.admissionDate),
      new Status(data.status),
      new Email(data.email),
      new CreatedAt(data.createdAt),
      new UpdatedAt(data.updatedAt)
    );
  }*/

  static createFromArray(data: any[]): UserEntity[] {
    return data.map((user) => {
      const completeNameUser = {
        firstName: user.firstName,
        otherName: user.otherName,
        firstSurname: user.firstSurname,
        lastSurname: user.lastSurname,
      };
      const userIdentification = {
        identificationNumber: user.identificationNumber,
        identificationType: user.identificationType,
      };

      return new this(
        user.id ? new Id(user.id) : new Id(""),
        CompleteNameUser.create({ ...completeNameUser }),
        CountryEntity.create({ ...user.employmentCountry }),
        UserIdentification.create({ ...userIdentification }),
        AreaEntity.create({ ...user.area }),
        new AdmissionDate(user.admissionDate),
        new Status(user.status),
        new Email(user.email),
        new CreatedAt(user.createdAt),
        new UpdatedAt(user.updatedAt)
      );
    });
  }

  static createToSave(data: any): UserEntity {
    return new this(
      data.id ? new Id(data.id) : new Id(""),
      CompleteNameUser.create({ ...data.completeNameUser }),
      CountryEntity.create({ ...data.country }),
      UserIdentification.create({ ...data.userIdentification }),
      AreaEntity.create({ ...data.area }),
      new AdmissionDate(data.admissionDate),
      new Status(data.status),
      undefined,
      data.createdAt && new CreatedAt(data.createdAt),
      data.updatedAt && new UpdatedAt(data.updatedAt)
    );
  }

  generateEmail(data: string): void {
    if (this.email.getValue() !== "") return;

    const { firstName, firstSurname } = this.completeNameUser.toCreateEmail();
    const { domain } = this.employmentCountry.toJson();
    this.email = new Email(`${firstName}.${firstSurname}${data}@${domain}`);
  }

  toValidateIfExistEmail() {
    const { firstName, firstSurname } = this.completeNameUser.toCreateEmail();
    const { domain } = this.employmentCountry.toJson();
    return {
      partialEmail: `${firstName}.${firstSurname}`,
      domain,
    };
  }

  toSaveInDb() {
    return {
      ...this.completeNameUser.toJson(),
      email: this.email.getValue(),
      employmentCountry: this.employmentCountry.toJson()
        .id as unknown as number,
      area: this.area.toJson().id as unknown as number,
      ...this.userIdentification.toJson(),
      admissionDate: this.admissionDate.getValue(),
      status: this.status.getValue(),
    };
  }

  static toJsonArray(data: UserEntity[]) {
    return data.map((user) => {
      return {
        ...user.toJson(),
      };
    });
  }

  toUpdateInDb() {
    const data = {
      id: this.id.getValue(),
      ...this.completeNameUser.toJson(),
      email: this.email.getValue(),
      employmentCountry: this.employmentCountry.toJson().id,
      area: this.area.toJson().id,
      ...this.userIdentification.toJson(),
      admissionDate: this.admissionDate.getValue(),
      status: this.status.getValue(),
    };
    const object: any = {};
    Object.entries(data).forEach((data) => {
      if (data[1] === undefined || data[1] === null) return;
      const a: any = {};
      a[data[0]] = data[1];
      Object.assign(object, a);
    });

    return object;
  }

  toJson() {
    return {
      id: this.id.getValue(),
      ...this.completeNameUser.toJson(),
      email: this.email.getValue(),
      employmentCountry: this.employmentCountry.toJson(),
      area: this.area.toJson(),
      ...this.userIdentification.toJson(),
      admissionDate: this.admissionDate.getValue(),
      status: this.status.getValue(),
      updatedAt: this.updatedAt?.getValue(),
      createdAt: this.createdAt?.getValue(),
    };
  }
}
