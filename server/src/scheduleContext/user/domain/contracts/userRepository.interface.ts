import { UserEntity } from "../user.entity";
import { Id } from "../valueObjects";

export interface UserRepository {
  create(user: UserEntity): void;
  edit(user: UserEntity): void;
  delete(id: Id): void;
  getAll(): Promise<UserEntity[]>;
  validateEmail(validation: any): Promise<any>;
  validateIdentification(validation: any): Promise<any>;
}
