export interface CompleteNameUserDTO {
  firstName: string;
  otherName: string;
  firstSurname: string;
  lastSurname: string;
}

export interface UserIdentificationDTO {
  identificationType: string;
  identificationNumber: string;
}

export interface CreateUserDTO {
  completeNameUser: CompleteNameUserDTO;
  userIdentification: UserIdentificationDTO;
  status: boolean;
  countryId: string;
  areaId: string;
  admissionDate?: Date;
}

export interface EditUserDTO extends CreateUserDTO {
  id: string;
}

export interface UserDTO {
  id: string;
  completeNameUser: CompleteNameUserDTO;
  status: boolean;
}
