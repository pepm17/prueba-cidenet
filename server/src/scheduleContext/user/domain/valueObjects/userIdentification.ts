import { UserIdentificationDTO } from "..";

export class UserIdentification {
  constructor(private type: string, private value: string) {}

  static create(data: UserIdentificationDTO): UserIdentification {
    return new this(data.identificationType, data.identificationNumber);
  }

  toJson(): UserIdentificationDTO {
    return {
      identificationType: this.type,
      identificationNumber: this.value,
    };
  }
}
