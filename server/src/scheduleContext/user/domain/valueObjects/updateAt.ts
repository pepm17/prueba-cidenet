import { DateValueObject } from "../../../../shared/domain/valueObjects";

export class UpdatedAt extends DateValueObject {}
