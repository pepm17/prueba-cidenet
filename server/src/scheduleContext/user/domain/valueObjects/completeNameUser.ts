import { CompleteNameUserDTO } from "../user.dto";

export class CompleteNameUser {
  constructor(
    private firstName: string,
    private otherName: string,
    private firstSurname: string,
    private lastSurname: string
  ) {}

  static create(data: CompleteNameUserDTO): CompleteNameUser {
    return new this(
      data.firstName,
      data.otherName,
      data.firstSurname,
      data.lastSurname
    );
  }

  private firstNameToLowerCase() {
    return this.firstName.toLowerCase();
  }

  private firstSurNameDeleteSpaceAndConvertToLowerCase() {
    return this.firstSurname.replace(/ /g, "").toLowerCase();
  }

  toCreateEmail(): CompleteNameUserDTO {
    return {
      firstName: this.firstNameToLowerCase(),
      otherName: this.otherName,
      firstSurname: this.firstSurNameDeleteSpaceAndConvertToLowerCase(),
      lastSurname: this.lastSurname,
    };
  }

  toJson(): CompleteNameUserDTO {
    return {
      firstName: this.firstName,
      otherName: this.otherName,
      firstSurname: this.firstSurname,
      lastSurname: this.lastSurname,
    };
  }
}
