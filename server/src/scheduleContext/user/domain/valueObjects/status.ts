import { BooleanValueObject } from "../../../../shared/domain/valueObjects";

export class Status extends BooleanValueObject {}
