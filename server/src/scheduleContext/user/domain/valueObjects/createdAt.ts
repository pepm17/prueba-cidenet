import { DateValueObject } from "../../../../shared/domain/valueObjects";

export class CreatedAt extends DateValueObject {}
