import {
  IsBoolean,
  IsDate,
  IsDateString,
  IsString,
  Length,
} from "class-validator";

export class CreateUserValidator {
  @IsString({ message: "First name must be a string" })
  @Length(1, 20, {
    message: "First name must be a minor to 20",
  })
  firstName!: string;

  @Length(0, 50, {
    message: "Other name must be a minor to 50",
  })
  othersName!: string;

  @IsString({ message: "First surname must be a string" })
  @Length(1, 20, {
    message: "First surname must be a minor to 20",
  })
  firstSurname!: string;

  @IsString({ message: "Last surname must be a string" })
  @Length(1, 20, {
    message: "Last surname must be a minor to 20",
  })
  lastSurname!: string;

  @IsString({ message: "Identification number must be a string" })
  identificationType!: string;

  @IsString({ message: "Identification number must be a string" })
  @Length(1, 20, {
    message: "Identification number must be a minor to 20",
  })
  identificationNumber!: string;

  @IsBoolean({ message: "Status must be a boolean" })
  status!: boolean;

  @IsString({ message: "CountryId must be a string" })
  countryId!: string;

  @IsString({ message: "AreaId must be a string" })
  areaId!: string;

  @IsDateString({ message: "AreaId must be a date" })
  admissionDate!: Date;

  toJson() {
    return {
      firstName: this.firstName,
      othersName: this.othersName,
      firstSurname: this.firstSurname,
      lastSurname: this.lastSurname,
      identificationType: this.identificationType,
      identificationNumber: this.identificationNumber,
      status: this.status,
      countryId: this.countryId,
      areaId: this.areaId,
      admissionDate: this.admissionDate,
    };
  }
}
