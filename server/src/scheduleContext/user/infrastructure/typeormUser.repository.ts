import { Service } from "typedi";
import { getRepository, Repository } from "typeorm";
import { UserModel } from ".";
import { UserEntity } from "../domain";
import { UserRepository } from "../domain/contracts";
import { Id } from "../domain/valueObjects";

@Service("UserRepository")
export class TypeOrmUserRepository implements UserRepository {
  private repository: Repository<UserModel>;

  constructor() {
    this.repository = getRepository(UserModel);
  }
  edit(user: UserEntity): void {
    const { id, ...rest } = user.toUpdateInDb();
    this.repository.update(id, rest as unknown as UserModel);
  }
  delete(id: Id): void {
    this.repository.delete(id.getValue());
  }
  async getAll(): Promise<UserEntity[]> {
    const data = await this.repository
      .createQueryBuilder("users")
      .innerJoinAndSelect("users.area", "area")
      .innerJoinAndSelect("users.employmentCountry", "country")
      .getMany();
    return UserEntity.createFromArray(data);
  }

  async validateEmail(validation: any) {
    return await this.repository
      .createQueryBuilder("users")
      .select("users.email")
      .where("users.email like '%' || :email || '%'", {
        email: validation.partialEmail,
      })
      .andWhere("users.email like '%' || :email || '%'", {
        email: validation.domain,
      })
      .getMany();
  }

  async validateIdentification(validation: any) {
    return await this.repository
      .createQueryBuilder("users")
      .select("users.id")
      .where("users.identificationType = :identificationType", {
        identificationType: validation.identificationType,
      })
      .andWhere("users.identificationNumber = :identificationNumber", {
        identificationNumber: validation.identificationNumber,
      })
      .getOne();
  }

  create(user: UserEntity): void {
    this.repository.save(user.toSaveInDb() as unknown as UserModel);
  }
}
