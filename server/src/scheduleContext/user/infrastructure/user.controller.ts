import { CommandQueryBus } from "@Shared/domain/contracts";
import { TypediCommandQueryBus } from "@Shared/infrastructure";
import {
  Body,
  Delete,
  Get,
  JsonController,
  Params,
  Post,
  Put,
} from "routing-controllers";
import { Inject } from "typedi";
import { CreateUserCommand } from "../application/createUserUseCase";
import { DeleteUserCommand } from "../application/deleteUserUseCase";
import { EditUserCommand } from "../application/editUserUseCase";
import { GetAllUserQuery } from "../application/getAllUser";
import { CreateUserValidator } from "./validators";

@JsonController("/api/user")
export class UserController {
  constructor(
    @Inject(() => TypediCommandQueryBus)
    private readonly commandQueryBus: CommandQueryBus
  ) {}

  @Get("/")
  async getUsers() {
    const command = new GetAllUserQuery();
    const data = await this.commandQueryBus.handle(command);
    return { response: data };
  }

  @Post("/")
  async createUser(@Body() body: CreateUserValidator) {
    const command = CreateUserCommand.create(body.toJson());
    await this.commandQueryBus.handle(command);
    return { response: "User created Succesfully" };
  }

  @Put("/:id")
  async editUser(@Body() body: CreateUserValidator, @Params() id: string) {
    const command = EditUserCommand.create(body, id);
    await this.commandQueryBus.handle(command);
    return { response: "User edited Succesfully" };
  }

  @Delete("/:id")
  async deleteUser(@Params() id: string) {
    const command = new DeleteUserCommand(id);
    await this.commandQueryBus.handle(command);
    return { response: "User deleted Succesfully" };
  }
}
