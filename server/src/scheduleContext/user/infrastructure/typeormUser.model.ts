import { AreaModel } from "../../area/infrastructure";
import { CountryModel } from "../../country/infrastructure";
import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
} from "typeorm";

@Entity({ name: "users" })
export class UserModel {
  @PrimaryGeneratedColumn({ name: "id", type: "integer" })
  id!: string;

  @Column({ nullable: false })
  firstName!: string;

  @Column({ nullable: false })
  otherName!: string;

  @Column({ nullable: false })
  firstSurname!: string;

  @Column({ nullable: false })
  lastSurname!: string;

  @Column({ nullable: false })
  identificationType!: string;

  @Column({ nullable: false })
  identificationNumber!: string;

  @Column({ nullable: false })
  email!: string;

  @Column({ nullable: false })
  status!: boolean;

  @Column({ nullable: false })
  admissionDate!: Date;

  @ManyToOne((type) => CountryModel, (country) => country.users)
  employmentCountry!: CountryModel;

  @ManyToOne((type) => AreaModel, (area) => area.users)
  area!: AreaModel;

  @CreateDateColumn({ type: "timestamp", nullable: true })
  createdAt!: Date;

  @UpdateDateColumn({ type: "timestamp", nullable: true })
  updatedAt!: Date;
}
