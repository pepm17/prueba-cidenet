import { UserModel } from "../../user/infrastructure";
import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from "typeorm";

@Entity({ name: "countries" })
export class CountryModel {
  @PrimaryGeneratedColumn({ name: "id", type: "integer" })
  id!: string;

  @Column({ nullable: false })
  name!: string;

  @Column({ nullable: false })
  domain!: string;

  @OneToMany((type) => UserModel, (users) => users.employmentCountry)
  users!: UserModel[];

  @CreateDateColumn({ type: "timestamp", nullable: true })
  createdAt!: Date;

  @UpdateDateColumn({ type: "timestamp", nullable: true })
  updatedAt!: Date;
}
