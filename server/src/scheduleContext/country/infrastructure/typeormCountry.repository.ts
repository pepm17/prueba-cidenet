import { Service } from "typedi";
import { getRepository, Repository } from "typeorm";
import { CountryModel } from ".";
import { CountryEntity } from "../domain";
import { CountryRepository } from "../domain/contracts";
import { Id } from "../domain/valueObjects";

@Service("CountryRepository")
export class TypeOrmCountryRepository implements CountryRepository {
  private repository: Repository<CountryModel>;

  constructor() {
    this.repository = getRepository(CountryModel);
  }

  async getById(id: Id): Promise<CountryEntity | null> {
    const data = await this.repository.findOne(id.getValue());
    if (!data) return null;
    return CountryEntity.create(data);
  }
}
