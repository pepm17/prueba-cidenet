export interface CountryDTO {
  id: string;
  name: string;
  domain: string;
}
