import { CountryDTO } from "./country.dto";
import { Domain, Name, Id } from "./valueObjects";

export class CountryEntity {
  constructor(private id: Id, private name: Name, private domain: Domain) {}

  static create(country: CountryDTO): CountryEntity {
    return new this(
      new Id(country.id),
      new Name(country.name),
      new Domain(country.domain)
    );
  }

  toJson(): CountryDTO {
    return {
      id: this.id.getValue(),
      name: this.name.getValue(),
      domain: this.domain.getValue(),
    };
  }
}
