import { CountryEntity } from "../country.entity";
import { Id } from "../valueObjects";

export interface CountryRepository {
  getById(id: Id): Promise<CountryEntity | null>;
}
