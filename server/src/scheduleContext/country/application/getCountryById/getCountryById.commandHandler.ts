import { CountryEntity } from "../../../country/domain";
import { Id } from "../../../country/domain/valueObjects";
import { Handler } from "../../../../shared/domain/contracts";
import { Inject, Service } from "typedi";
import { GetCountryByIdCommand, GetCountryByIdUseCase } from ".";

@Service("GetCountryByIdCommandHandler")
export class GetCountryByIdCommandHandler implements Handler<CountryEntity> {
  constructor(
    @Inject("GetCountryByIdUseCase")
    private getCountryByIdUseCase: GetCountryByIdUseCase
  ) {}

  async handler(
    getCountryByIdCommand: GetCountryByIdCommand
  ): Promise<CountryEntity> {
    const { id } = getCountryByIdCommand.toJson();
    return this.getCountryByIdUseCase.getCountryById(new Id(id));
  }
}
