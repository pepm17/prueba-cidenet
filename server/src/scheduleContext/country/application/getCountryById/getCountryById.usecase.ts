import { CountryRepository } from "../../domain/contracts";
import { Id } from "../../domain/valueObjects";
import { Inject, Service } from "typedi";

@Service("GetCountryByIdUseCase")
export class GetCountryByIdUseCase {
  constructor(
    @Inject("CountryRepository") private countryRepository: CountryRepository
  ) {}

  async getCountryById(id: Id) {
    const data = await this.countryRepository.getById(id);
    if (!data) throw new Error("Error");
    return data;
  }
}
