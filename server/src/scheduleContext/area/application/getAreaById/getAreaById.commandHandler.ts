import { AreaEntity } from "../../domain";
import { Id } from "../../domain/valueObject";
import { Handler } from "../../../../shared/domain/contracts";
import { Inject, Service } from "typedi";
import { GetAreaByIdCommand, GetAreaByIdUseCase } from ".";

@Service("GetAreaByIdCommandHandler")
export class GetAreaByIdCommandHandler implements Handler<AreaEntity> {
  constructor(
    @Inject("GetAreaByIdUseCase")
    private getAreaByIdUseCase: GetAreaByIdUseCase
  ) {}

  async handler(getAreaByIdCommand: GetAreaByIdCommand): Promise<AreaEntity> {
    const { id } = getAreaByIdCommand.toJson();
    return this.getAreaByIdUseCase.getAreaById(new Id(id));
  }
}
