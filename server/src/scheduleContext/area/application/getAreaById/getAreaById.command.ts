import { Command } from "../../../../shared/domain/contracts";

export class GetAreaByIdCommand implements Command {
  constructor(private id: string) {}

  static create(data: any) {
    return new this(data.id);
  }

  toJson() {
    return {
      id: this.id,
    };
  }
}
