import { AreaRepository } from "../../domain/contracts";
import { Id } from "../../domain/valueObject";
import { Inject, Service } from "typedi";

@Service("GetAreaByIdUseCase")
export class GetAreaByIdUseCase {
  constructor(
    @Inject("AreaRepository") private areaRepository: AreaRepository
  ) {}

  async getAreaById(id: Id) {
    const data = await this.areaRepository.getById(id);
    if (!data) throw new Error("Error");
    return data;
  }
}
