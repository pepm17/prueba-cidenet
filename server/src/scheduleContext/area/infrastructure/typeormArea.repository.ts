import { Service } from "typedi";
import { getRepository, Repository } from "typeorm";
import { AreaEntity } from "../domain";
import { AreaRepository } from "../domain/contracts";
import { Id } from "../domain/valueObject";
import { AreaModel } from "./typeormArea.model";

@Service("AreaRepository")
export class TypeOrmAreaRepository implements AreaRepository {
  private repository: Repository<AreaModel>;

  constructor() {
    this.repository = getRepository(AreaModel);
  }

  async getById(id: Id): Promise<AreaEntity | null> {
    const data = await this.repository.findOne(id.getValue());
    if (!data) return null;
    return AreaEntity.create(data);
  }
}
