import { UserModel } from "../../user/infrastructure";
import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from "typeorm";

@Entity({ name: "area" })
export class AreaModel {
  @PrimaryGeneratedColumn({ name: "id", type: "integer" })
  id!: string;

  @Column({ nullable: false })
  name!: string;

  @OneToMany((type) => UserModel, (users) => users.area)
  users!: UserModel[];

  @CreateDateColumn({ type: "timestamp", nullable: true })
  createdAt!: Date;

  @UpdateDateColumn({ type: "timestamp", nullable: true })
  updatedAt!: Date;
}
