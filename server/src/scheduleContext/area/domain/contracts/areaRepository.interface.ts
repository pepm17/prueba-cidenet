import { AreaEntity } from "../area.entity";
import { Id } from "../valueObject";

export interface AreaRepository {
  getById(id: Id): Promise<AreaEntity | null>;
}
