export interface AreaDTO {
  id: string;
  name: string;
}
