import { AreaDTO } from ".";
import { Id, Name } from "./valueObject";

export class AreaEntity {
  constructor(private id: Id, private name: Name) {}

  static create(data: AreaDTO): AreaEntity {
    return new this(new Id(data.id), new Name(data.name));
  }

  toJson(): AreaDTO {
    return {
      id: this.id.getValue(),
      name: this.name.getValue(),
    };
  }
}
