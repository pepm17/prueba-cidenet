import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models';
import { UserService } from 'src/app/services/user.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  validateForm!: FormGroup;
  users: UserModel[] = [];
  showModal: boolean = false;
  titleModal: string = '';
  create: boolean = true;
  fields = [
    { field: 'First Name', data: 'firstName' },
    { field: 'Others Name', data: 'otherName' },
    { field: 'First Surname', data: 'firstSurname' },
    { field: 'Second Surname', data: 'lastSurname' },
    { field: 'Email', data: 'email' },
    { field: 'Identification Type', data: 'identificationType' },
    { field: 'Identification Number', data: 'identificationNumber' },
    { field: 'Country', data: 'employmentCountry' },
    { field: 'Area', data: 'area' },
    { field: 'Status', data: 'status' },
    { field: 'Admision Date', data: 'admissionDate' },
    { field: 'Created At', data: 'createdAt' },
    { field: 'Updated At', data: 'updatedAt' },
    { field: 'Edit', nzRigth: true },
    { field: 'Delete', nzRigth: true },
  ];
  hiddenComponent: boolean = false;
  constructor(
    private userService: UserService,
    private nzMessageService: NzMessageService,
    private formBuilder: FormBuilder
  ) {}

  async ngOnInit(): Promise<void> {
    this.users = await this.userService.getAllUser();
  }

  async ngOnChanges(): Promise<void> {}

  addUser(): void {
    this.create = true;
    this.hiddenComponent = true;
    this.titleModal = 'Create User';
    this.initForm();
    this.showModal = true;
  }

  editUser(user: UserModel): void {
    this.hiddenComponent = false;

    this.create = false;
    this.titleModal = 'Edit User';
    this.dataInForm(user);
    this.showModal = true;
  }

  async onSubmitCreateUser(): Promise<void> {
    const message = await this.userService.createUser(this.validateForm.value);
    this.nzMessageService.create('success', message);
    setTimeout(async () => {
      await this.ngOnInit();
    }, 1000);
  }

  async onSubmitEditUser(): Promise<void> {
    const message = await this.userService.updateUser(
      this.validateForm.value,
      this.validateForm.value.id
    );
    this.nzMessageService.create('success', message);
    setTimeout(async () => {
      await this.ngOnInit();
    }, 1000);
  }

  showModalHandle() {
    return () => (this.showModal = !this.showModal);
  }

  private initForm(): void {
    this.validateForm = this.formBuilder.group({
      id: ['', [Validators.required]],
      firstName: ['', [Validators.required, Validators.maxLength(20)]],
      otherName: ['', [, Validators.maxLength(50)]],
      firstSurname: ['', [Validators.required, , Validators.maxLength(20)]],
      lastSurname: ['', [Validators.required, , Validators.maxLength(20)]],
      identificationType: ['', [Validators.required]],
      identificationNumber: ['', [Validators.required]],
      employmentCountry: ['', [Validators.required]],
      area: ['', [Validators.required]],
      admissionDate: ['', [Validators.required]],
      status: ['true', [Validators.required]] as unknown as boolean,
    });
  }

  private dataInForm(user: UserModel): void {
    this.validateForm = this.formBuilder.group({
      id: [user.id, [Validators.required]],
      firstName: [user.firstName, [Validators.required]],
      otherName: [user.otherName, []],
      firstSurname: [user.firstSurname, [Validators.required]],
      lastSurname: [user.lastSurname, [Validators.required]],
      identificationType: [user.identificationType, [Validators.required]],
      identificationNumber: [user.identificationNumber, [Validators.required]],
      employmentCountry: [user.employmentCountry.id, [Validators.required]],
      area: [user.area.id, [Validators.required]],
      admissionDate: [user.admissionDate, [Validators.required]],
      status: [user.status, [Validators.required]] as unknown as boolean,
    });
  }

  async deleteData(data: string): Promise<void> {
    const message = await this.userService.deleteUserById(data);
    this.nzMessageService.create('success', message);
    setTimeout(async () => {
      await this.ngOnInit();
    }, 1000);
  }
}
