import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UserModel } from 'src/app/models';

interface Field {
  field: string;
  data?: string;
  nzRigth?: boolean;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input()
  users: UserModel[] = [];
  @Input()
  fields: Field[] = [];
  @Input()
  functionDelete!: (data: string) => Promise<void>;
  @Input()
  functionEdit!: (user: UserModel) => void;

  listPagination: number = 10;
  visible = false;
  dataToFind = '';
  searchValue = '';

  listOfDisplayData = [...this.users];

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    this.listOfDisplayData = [...this.users];
  }

  selectData(data?: string): void {
    this.visible = data ? true : false;
    this.dataToFind = data || '';
    console.log(this.dataToFind);
  }

  search(): void {
    this.visible = false;
    this.listOfDisplayData = this.users.filter(
      (item: any) => item[this.dataToFind].indexOf(this.searchValue) !== -1
    );
  }

  reset(): void {
    this.searchValue = '';
    this.search();
  }
}
