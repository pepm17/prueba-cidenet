import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalWriteDataComponent } from './modal-write-data.component';

describe('ModalWriteDataComponent', () => {
  let component: ModalWriteDataComponent;
  let fixture: ComponentFixture<ModalWriteDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalWriteDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalWriteDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
