import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { differenceInCalendarDays, setHours } from 'date-fns';

@Component({
  selector: 'app-modal-write-data',
  templateUrl: './modal-write-data.component.html',
  styleUrls: ['./modal-write-data.component.scss'],
})
export class ModalWriteDataComponent implements OnInit {
  @Input()
  isVisible: boolean = false;
  @Input()
  isVisibleMethod!: () => boolean;
  @Input()
  validateForm!: FormGroup;
  @Input()
  submitFormOnSave!: () => Promise<void>;
  @Input()
  title: string = '';
  @Input()
  hiddenComponent: boolean = true;
  constructor() {}

  ngOnInit(): void {}

  async handleOk() {
    this.validateForm.value.status =
      this.validateForm.value.status === 'true' ? true : false;
    await this.submitFormOnSave();
    this.isVisibleMethod();
  }
  handleCancel() {
    this.isVisibleMethod();
  }
  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  disabledDate = (current: Date): boolean => {
    const today = new Date();
    return (
      differenceInCalendarDays(current, today) < -30 ||
      differenceInCalendarDays(current, today) > 0
    );
  };
}
