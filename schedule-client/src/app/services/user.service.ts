import { Injectable } from '@angular/core';
import { UserModel, UserToCreateModel, UserToCreateModelForm } from '../models';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  private urlApi: string;

  constructor(private http: HttpClient) {
    this.urlApi = 'http://localhost:4000/api/user';
  }

  async getAllUser(): Promise<UserModel[]> {
    const data = await this.http.get<any>(this.urlApi).toPromise();
    return data.response.map((u: UserModel) => {
      u.admissionDate = new Date(u.admissionDate);
      u.createdAt = new Date(u.createdAt);
      u.updatedAt = new Date(u.updatedAt);
      return u;
    });
  }

  async deleteUserById(id: string): Promise<string> {
    const data = await this.http
      .delete<any>(`${this.urlApi}/${id}`)
      .toPromise();
    return data.response;
  }

  async createUser(user: UserToCreateModelForm): Promise<string> {
    const userToSave: UserToCreateModel = {
      ...user,
      othersName: user.otherName,
      areaId: user.area,
      countryId: user.employmentCountry,
      admissionDate: new Date(user.admissionDate),
    };
    try {
      const data = await this.http
        .post<any>(`${this.urlApi}`, userToSave)
        .toPromise();
      return data.response;
    } catch (error) {
      console.log(error);
      return 'Error with data';
    }
  }

  async updateUser(user: UserToCreateModelForm, id: string): Promise<string> {
    console.log(user);
    const userToUpdate: UserToCreateModel = {
      ...user,
      othersName: user.otherName,
      areaId: user.area,
      countryId: user.employmentCountry,
    };
    try {
      const data = await this.http
        .put<any>(`${this.urlApi}/${id}`, userToUpdate)
        .toPromise();
      return data.response;
    } catch (error) {
      console.log(error);
      return 'Error with data';
    }
  }
}
