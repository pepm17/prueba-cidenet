export interface Country {
  id: string;
  name: string;
  domain: string;
}
