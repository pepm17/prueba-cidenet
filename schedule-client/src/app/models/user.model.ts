import { Area } from './area';
import { Country } from './country';

export interface UserModel {
  id: string;
  firstName: string;
  otherName: string;
  firstSurname: string;
  lastSurname: string;
  identificationType: string;
  identificationNumber: string;
  employmentCountry: Country;
  area: Area;
  admissionDate: Date;
  email: string;
  status: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface UserToCreateModelForm {
  id: string;
  firstName: string;
  otherName: string;
  firstSurname: string;
  lastSurname: string;
  identificationType: string;
  identificationNumber: string;
  employmentCountry: string;
  area: string;
  admissionDate: Date;
  email: string;
  status: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface UserToCreateModel {
  id: string;
  firstName: string;
  othersName: string;
  firstSurname: string;
  lastSurname: string;
  identificationType: string;
  identificationNumber: string;
  countryId: string;
  areaId: string;
  admissionDate: Date;
  email: string;
  status: boolean;
  createdAt: Date;
  updatedAt: Date;
}
